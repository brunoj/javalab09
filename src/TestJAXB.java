import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class TestJAXB {

    public static void main(String... arg) throws Exception {
        faktura code = new faktura();
        code.setDanePlatnika("Bruno Jamula");
        code.setDaneSprzedaawcy("Super Wiqster");
        code.setNumerFaktury("2131231312");
        code.setListaTowarow("Auto");
        code.setPodsumowanie("super podsumowanie");
        JAXBContext jaxbContext = JAXBContext.newInstance(faktura.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        //WYŚWIETLENIE NA OUT
        jaxbMarshaller.marshal(code, System.out);

        //ZAPIS DO PLIKU
        jaxbMarshaller.marshal(code, new File("C:\\Users\\pas217\\Downloads\\faktura.xml"));


        File file = new File("C:\\Users\\pas217\\Downloads\\faktura.xml");

        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        faktura codeFaktura = (faktura) jaxbUnmarshaller.unmarshal(file);

        System.out.println("Kupujacy: " + codeFaktura.getDanePlatnika());
        System.out.println("Sprzedawca: " + codeFaktura.getDaneSprzedaawcy());
    }
}