import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name="faktury")
public class faktura {
    private List<faktura> faktury = new ArrayList<faktura>();
    String daneSprzedaawcy;
    String listaTowarow;
    String podsumowanie;
    String numerFaktury;
    String danePlatnika;
    public faktura(String daneSprzedaawcy, String listaTowarow,  String podsumowanie, String numerFaktury , String danePlatnika) {
        this.daneSprzedaawcy = daneSprzedaawcy;
        this.danePlatnika = danePlatnika;
        this.listaTowarow = listaTowarow;
        this.danePlatnika = danePlatnika;
        this.podsumowanie = podsumowanie;
        this. numerFaktury =  numerFaktury;

    }

    public void add(faktura faktury) {
        this.faktury.add(faktury);
    }

    @XmlElements(@XmlElement(name="pozycja"))
    public List<faktura> getPostCodes() {
        return faktury;
    }

    @XmlAttribute(name="numerFakturu")
    public String getNumerFaktury() {
        return numerFaktury;
    }

    public void setNumerFaktury(String numerFaktury) {
        this.numerFaktury = numerFaktury;
    }

    @XmlAttribute(name="podsumowanie")

    public String getPodsumowanie() {
        return podsumowanie;
    }

    public void setPodsumowanie(String podsumowanie) {
        this.podsumowanie = podsumowanie;
    }

    @XmlAttribute(name="listaTowaró")

    public String getListaTowarow() {

        return listaTowarow;
    }

    public void setListaTowarow(String listaTowarow) {
        this.listaTowarow = listaTowarow;
    }

    @XmlAttribute(name="danePlatnika")
    public String getDanePlatnika() {

        return danePlatnika;
    }

    public void setDanePlatnika(String danePlatnika) {
        this.danePlatnika = danePlatnika;
    }


    @XmlAttribute(name="daneSprzedawcy")

    public String getDaneSprzedaawcy() {

        return daneSprzedaawcy;
    }

    public void setDaneSprzedaawcy(String daneSprzedaawcy) {
        this.daneSprzedaawcy = daneSprzedaawcy;
    }



}
